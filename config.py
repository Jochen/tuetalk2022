REVEAL_META = {
    # Title of the slide
    "title": "Coherent space communications and Tx impairments",
    # Author in the metadata of the slide
    "author": "Jochen Schröder",
    # Description in the metadata of the slide
    "description": "Seminar at TuE 2022",
}


REVEAL_PLUGINS = ["RevealMenu", 
                    "RevealMarkdown",
                   "RevealHighlight",
                   "RevealSearch",
                   "RevealNotes",
                   "RevealMath",
                   "RevealZoom"]
REVEAL_EXTRA_SCRIPTS = [ "plugins/menu/menu.js"]
